function printTringle(rows,chr){
    for(let i=1;i<=rows;i++)
        console.log(chr.repeat(i))
}

printTringle(7,"#")
console.log()

function FizzBuzz(min,max){
    for(let i=min;i<=max;i++){
        if(i % 3 == 0)
            console.log("Fizz")
        else if(i % 5 == 0)
            console.log("Buzz")
        else
            console.log(i)
    }
}

FizzBuzz(1,100)
console.log()


function FizzBuzz2(min,max){
    for(let i=min;i<=max;i++){
        if(i % 5 == 0 && i % 3 == 0)
            console.log("FizzBuzz")
        else if(i % 3 == 0)
            console.log("Fizz")
        else if(i % 5 == 0)
            console.log("Buzz")
        else
            console.log(i)
    }
}

FizzBuzz2(1,100)
console.log()


function cheesBoard(size = 8){
   for(let i=0;i<size;i++){
        let string = ""
        for(let j=0;j<size;j++)
            i%2 == j%2? string+=" ":string+="#"
        console.log(string) 
    }    
}

cheesBoard()
console.log()

function cheesBoard2(width = 8,height){
    height = height? height : width
    for(let i=0;i<height;i++){
        let string = ""
        for(let j=0;j<width;j++)
            i%2 == j%2? string+=" ":string+="#"
        console.log(string)
    }
}

cheesBoard2(10)